<?php

namespace Cryptaeve\Seat\SeatBills\Http\Controllers;

use Seat\Web\Http\Controllers\Controller;

/**
 * Class HomeController
 * @package cryptaeve\Seat\Seat-Bills\Http\Controllers
 */
class HomeController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function getHome()
    {

        return view('seatbills::mybills');
    }

}
