<?php

return [

    // Integrating with the SeAT menu is defined here.
    // Refer to the web package for a structure reference.
    'seat-bills' => [
        'name' => 'Billing',
        'icon' => 'fa-btc',
        'route_segment' => 'seat-bills',
        'entries' => [
            [
                'name' => 'My Bills',
                'icon' => 'fa-coins',
                'route' => 'home'
            ],
            [
                'name' => 'Issue Bill',
                'icon' => 'fa-cart-plus',
                'route' => 'home'
            ],
            [
                'name' => 'Create Recurring Bill',
                'icon' => 'fa-credit-card',
                'route' => 'home'
            ],


        ]
    ]

];
